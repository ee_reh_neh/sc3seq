#!/bin/bash

# IGR 16/08/10
# File: extract_coverage_featureCounts.sh

# Launches featureCounts for all files, separately by species, generates matrices of results both clean and dirty. 

# $1: Path to mapped reads (gzipped fastq files)
# $2: Output directory name

if [ $# -ne 0 ]; then
    echo "USAGE: extract_coverage_featureCounts.sh"
    exit 1
fi

bamdirectory="/home/users/ntu/igr/pera_et_al/mapped"
outdir="/home/users/ntu/igr/pera_et_al/rpkm"

# Location of reference file roots:
macFasOrtho="/home/users/ntu/igr/orthoexon/hg38v86_macFas5_ortho/macFas5_ensembl86_orthoexon_"
humanOrtho="/home/users/ntu/igr/orthoexon/hg38v86_macFas5_ortho/hg38_ensembl86_orthoexon_"

# Create directories for the output
if [ ! -d $outdir/logs ]; then
    mkdir -p $outdir/logs
fi

# Launch featurecounts for either species at the multiple thresholds
# This loop can be commented out when the time comes. 
for i in 0.9 0.91 0.92 0.93 0.94 0.95; do 
    
    # Macaques:
    echo -e "#! /bin/bash

    echo 'Launching featureCounts for all macFas samples.'
    if [ -s $outdir/macFas_counts_matrix_${i}.out ]; then
        echo 'featureCounts has already been run for this species.'
    else
        # Launch featureCounts
        # Building the command:
        # -T: number of threads, set to 8 by me. 
        # -F: format of the feature file; set to SAF. See the manual for format specification. File is generated automatically by blat_processing_intraspecies.R
        # -a: annotation file name - here, species-specific ortho exon file.
        # -s: assign strandedness to data set. Set to 0, first strand, on the basis of library construction. 
        # -o: sets output file name
        # -f: count things at the exon level rather than the gene level.

        longList=\`ls \$bamdirectory\/*bam\`

        featureCounts -f -T 8 -F SAF -a ${macFasOrtho}${i}_ortho_FALSE_pc.txt -s 0 -o $outdir/macFas_${i}pc_ortho_FALSE_genes.out \$longList        
        featureCounts -f -T 8 -F SAF -a ${macFasOrtho}${i}_ortho_FALSE_pc.txt -s 0 -f -o $outdir/macFas_${i}pc_ortho_FALSE_exons.out \$longList        
        # Clean up output for R
        # sed 's:\/::g' $outdir/macFas_${i}pc_ortho_FALSE.out | sed \"s/homeusersntuigrorthoexonsimNGShg38_macFas5polyesterortho_FALSE_${i}mappedmacFas5//g\" | sed 's/\.fastaaccepted_hits\.bam//g' > $outdir/macFas_${i}pc_ortho_FALSE_clean.out
        exitstatus=( \$? )
        if [ \${exitstatus}  != 0 ]; then
            echo ERROR: Failed featureCounts with exit code \${exitstatus}
            exit \${exitstatus}
        else 
            echo Successfully ran featureCounts for all macFas samples.
        fi
    fi
    " | qsub -l walltime=5:00:00,ncpus=8 -o $outdir/logs/macFas_${i}pc_FALSE_fc.out -N fcMacFas_${i} -V -j oe

done
