#!/bin/bash
# From a script by John Blischak
# Modified by IGR to work on PBSPro (NSCC)
# 2017.02.04

set -e

FILE=$1
BASE=`basename ${FILE}`
INPUTDIR=`dirname $FILE`
INDEXDIR=~/pera_et_al/indexes/
SPECIES=macFas5
OUTDIR=~/pera_et_al/mapped/
COLORCONVERT=T
THREADS=2
MISMATCH=2
INDEL=2

mkdir -p $OUTDIR

if [ ! -s $FILE ]
then
  echo "File is missing or empty: $FILE"
  exit 65
fi

if [ -s $OUTDIR/$FILE.bam ]
then
  echo "Output file already exists: $OUTDIR/$FILEfastq.gz.bam"
  exit 64
fi

# Load the right version of gcc to work with R-3.2.2

module load gcc
~/bin/R-3.2.2/bin/Rscript --vanilla ~/repos/sc3seq/subread_align.r $INPUTDIR $BASE $INDEXDIR $SPECIES $OUTDIR $COLORCONVERT $THREADS $MISMATCH $INDEL

# Count how many mapped reads, since the log files are not written correctly in PBSPro
module load samtools 
samtools view -c ${OUTDIR}/${BASE}.bam > ${OUTDIR}/${BASE}.mapped.txt 


