### expression_eda.R
### For experimental analysis of RNA-seq data from human and crab-eating macaque neural progenitors. 
### Modelled closely on main_analysis_final.R from IGR's chimp-human iPSC paper
### IGR 2017.02.09

### 0. PREPARE WORKSPACE. ###
### 1. READ IN COUNT MATRICES AND EXON LISTS. ### 
### 2. CALCULATE NORMALISED LIBRARY SIZES, COMPUTE CPM, FILTER, LOESS NORMALISE, AND COMPUTE RPKM. ###
### 3. REPRODUCIBILITY, RAREFACTION AND PCA PLOTS ###
### 4. DIFFERENTIAL EXPRESSION TESTING. ###

#############################
### 0. PREPARE WORKSPACE. ###
#############################
# extraVars <- commandArgs(trailingOnly=T)
extraVars <- c("ortho_FALSE", 0.92) 
.libPaths("~/R-3.2.2_libs/")

### load libraries
library(gplots) # 3.0.1
library(RColorBrewer) # 1.1-2
library(limma) # 3.26.9
library(edgeR) # 3.12.1
library(plyr) # 1.8.4

options(width=200)
setwd("/home/users/ntu/igr/pera_et_al/rpkm")

pal <- brewer.pal(10, "Set3")

#################################################
### 1. READ IN COUNT MATRICES AND EXON LISTS. ### 
#################################################

mfGeneCounts <- read.table(paste0("macFas_", extraVars[2], "pc_", extraVars[1], "_genes_clean.out"), header=T)
GeneCounts <- mfGeneCounts[,c(1,6:dim(mfGeneCounts)[2])]
dim(GeneCounts)
# [1] 34142  2528

countsNames <- colnames(GeneCounts[,3:dim(GeneCounts)[2]])

#prepare meta information from sample names and reorder:
samplesMeta <- read.table("sample_sheet.txt", header=T)
samplesMeta$stageCol <- pal[as.numeric(as.factor(samplesMeta$developmental_stage_s))]
samplesMeta$cex <- 0.9
samplesMeta$pch <- 16
samplesMeta <- samplesMeta[order(samplesMeta$Run_s),] # doublecheck this ordering

# Collapse to cell level
samplesMetaInd <- ddply(samplesMeta[,2:ncol(samplesMeta)], .(SRA_Sample_s), function(x) unique(x))

# Write table of mapped read depths
# mappedReads <- apply(GeneCounts[3:dim(GeneCounts)[2]], 2, sum)
# write.table(mappedReads, "all_featurecounts.txt", quote=F, col.names=F)

######################################################################################################
### 2. CALCULATE NORMALISED LIBRARY SIZES, COMPUTE CPM, FILTER, LOESS NORMALISE, AND COMPUTE RPKM. ###
######################################################################################################

# Summarise reads at the individual level not the SRA read level:

tGeneCounts <- data.frame(t(GeneCounts[,3:dim(GeneCounts)[2]]))
tGeneCounts$ind <- rownames(tGeneCounts)

tGeneCounts$ind <- samplesMeta$SRA_Sample_s

tbySample <- ddply(tGeneCounts, .(ind), function(x) colSums(x[,1:(ncol(x)-1)]))

bySample <- data.frame(t(tbySample[,2:ncol(tbySample)]))
colnames(bySample) <- tbySample$ind
bySample <- data.frame(GeneCounts$Geneid, bySample, GeneCounts$Length)
names(bySample)[1] <- "Geneid"
names(bySample)[ncol(bySample)] <- "Length"
write.table(bySample, file=paste0("macFas_", extraVars[2], "pc_", extraVars[1], "_genes_by_sample.out"), row.names=F, col.names=T, quote=F)

# reads into edgeR, calculate TMM and then CPM
bySampleDge <- DGEList(counts=as.matrix(bySample[,2:(ncol(bySample)-1)]), genes=bySample[,1])
bySampleDge <- calcNormFactors(bySampleDge)

### Calculate log2 CPM
cpmNorm <- cpm(bySampleDge, normalized.lib.sizes=TRUE, log=TRUE, prior.count=0.25) 
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_cpm.density_gene_clean.pdf"))
plotDensities(cpmNorm, group=samplesMetaInd$developmental_stage_s) 
abline(v=1)
dev.off()

# Filter on observing cpm greater or equal to 1 or more in at least half of the individuals in one species, not keeping library sizes.
bySampleDgeFilt <- bySampleDge[rowSums(cpmNorm >= 1) >= (ncol(cpmNorm)/100*20) , , keep.lib.sizes=F] 
dim(bySampleDgeFilt)
# [1] 9804  421

pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_cpmFilt.density_gene_clean.pdf"))
plotDensities(bySampleDgeFilt, group=samplesMetaInd$developmental_stage_s)
dev.off()

# Recalculate TMM and lib sizes
bySampleDgeFilt <- calcNormFactors(bySampleDgeFilt) ## recalculate norm factors
cpmNormFilt <- cpm(bySampleDgeFilt, normalized.lib.sizes=TRUE, log=TRUE, prior.count=0.25) 

# # Loess normalise the data
# ## For starters, Voom requires a design matrix as input
# design <- model.matrix(~ 0 + samplesMetaInd$species)
# colnames(design) <- c("human", "macFas")

# ## Voom on filtered nonGC normalized data, with cyclic loess normalization, and blocked by individual replicates - this requires two passes, one without the random individual effect and a second one that takes it into account - see the limma manual and this post and reply by Gordon Smyth!
# ## https://support.bioconductor.org/p/59700/

# ## note that cyclic loess is designed for between-array normalisation rather than RNA-seq, but it should still be usable. Ideally we would not need it, however, but I don't like the directionality of the results without normalisation, not to mention there's a clear outlier sample in there. 
# cpmNormLoess <- voom(bySampleDgeFilt, design, normalize.method="cyclicloess", plot=F) 
# pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_voom.loess.cpm.indrandom.output_clean.pdf"))
# dev.off()

# ## Turn this into RPKM
# # Values are already log2 CPM, so just need to substract
# rpkmNormLoess <- cpmNormLoess
# rpkmNormLoess$E[,1:10] <- cpmNormLoess$E[,1:10] - log2((bySample[rowSums(cpmNorm[,1:10] > 2) >= 5 | rowSums(cpmNorm[,11:20] > 2) >= 5 , 22])/1000)
# rpkmNormLoess$E[,11:20] <- cpmNormLoess$E[,11:20] - log2((bySample[rowSums(cpmNorm[,1:10] > 2) >= 5 | rowSums(cpmNorm[,11:20] > 2) >= 5 , 23])/1000)

# pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_voom.loess.rpkm.indrandom.density_clean.pdf"))
# plotDensities(rpkmNormLoess, group=samplesMeta$species) #again, a beautiful distribution, as expected. 
# dev.off()

#####################################################
### 3. REPRODUCIBILITY, RAREFACTION AND PCA PLOTS ###
#####################################################

# some barplots about mapping stats
# "Raw" library sizes:
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_mapped_reads_raw_gene_clean.pdf"))
mp <- barplot(sort(colSums(bySampleDge$counts)), ylab="Number of reads mapped to orthologous exons", xlab="", col="darkgrey", xaxt="n")
dev.off()

# normalised library sizes
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_mapped_reads_normalised_gene_clean.pdf"))
mp <- barplot(sort(bySampleDge$samples$lib.size * bySampleDge$samples$norm.factor), ylab="Normalized library sizes", xlab="", xaxt="n", col="darkgrey")
dev.off()

# number of genes expressed
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_number_of_genes_expressed_gene_clean.pdf"))
some.counts <- apply(bySampleDge$counts, 2, function(x) { sum(x > 0) })
mp <- barplot(sort(some.counts), ylab="Number of genes with at least 1 read", xlab="", xaxt="n", col="darkgrey")
dev.off()

# Perform rarefaction curves for number of expressed genes vs. proportion of pool mRNA
# As in Ramskold D, Wang ET, Burge CB, Sandberg R. 2009. An abundance of ubiquitously expressed genes revealed by tissue transcriptome sequence data. PLoS Comput Biol 5:e1000598.
# This gives an idea of the complexity of transcriptome in different tissues

# using ortho Exons genes aggregates
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_rarefaction_curves_gene_clean.pdf"))
plot(1:length(bySampleDge$counts[,1]), cumsum(sort(bySampleDge$counts[,1], decreasing=T)/sum(bySampleDge$counts[,1])), log="x", type="n", xlab="Number of genes", ylab="Fraction of reads pool", ylim=c(0,1)) ## initialize the plot area
for (sample in colnames(bySampleDge)){
  # lines(1:length(bySampleDge$counts[,sample]), cumsum(sort(bySampleDge$counts[,sample], decreasing=T)/sum(bySampleDge$counts[,sample])), col="darkgrey", lwd=2)
  lines(1:length(bySampleDge$counts[,sample]), cumsum(sort(bySampleDge$counts[,sample], decreasing=T)/sum(bySampleDge$counts[,sample])), col=as.character(samplesMetaInd[samplesMetaInd$SRA_Sample_s %in% sample,]$stageCol), lwd=2)
}
    legend(col=unique(samplesMetaInd$stageCol), legend=unique(samplesMetaInd$developmental_stage_s), lty=1, x="topleft", cex=0.75, bty="n")
dev.off()

# PCA plotting function:
plot.pca <- function(dataToPca, stageCol, namesPch, sampleNames){
    # check for invariant rows:
    dataToPca.clean <- dataToPca[!apply(dataToPca, 1, var) == 0,]
    pca <- prcomp(t(dataToPca.clean), scale=T, center=T)
    pca.var <- pca$sdev^2/sum(pca$sdev^2)
    plot(pca$x[,1], pca$x[,2], col=stageCol, pch=namesPch, cex=1, xlab=paste("PC1 (", round(pca.var[1]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC2 (", round(pca.var[2]*100, digits=2), "% of variance)", sep=""))
    legend(col=unique(stageCol), legend=sampleNames, pch=namesPch, x="bottomleft", cex=0.6, bty="n")
    plot(pca$x[,2], pca$x[,3], col=stageCol, pch=namesPch, cex=1, xlab=paste("PC2 (", round(pca.var[2]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC3 (", round(pca.var[3]*100, digits=2), "% of variance)", sep=""))
    legend(col=unique(stageCol), legend=sampleNames, pch=namesPch, x="topright", cex=0.6, bty="n")
    plot(pca$x[,3], pca$x[,4], col=stageCol, pch=namesPch, cex=1, xlab=paste("PC3 (", round(pca.var[3]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC4 (", round(pca.var[4]*100, digits=2), "% of variance)", sep=""))
    legend(col=unique(stageCol), legend=sampleNames, pch=namesPch, x="bottomright", cex=0.6, bty="n")
    plot(pca$x[,1], pca$x[,3], col=stageCol, pch=namesPch, cex=1, xlab=paste("PC1 (", round(pca.var[1]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC3 (", round(pca.var[3]*100, digits=2), "% of variance)", sep=""))
    legend(col=unique(stageCol), legend=sampleNames, pch=namesPch, x="bottomright", cex=0.6, bty="n")
    plot(pca$x[,2], pca$x[,4], col=stageCol, pch=namesPch, cex=1, xlab=paste("PC2 (", round(pca.var[2]*100, digits=2), "% of variance)", sep=""), ylab=paste("PC4 (", round(pca.var[4]*100, digits=2), "% of variance)", sep=""))
    legend(col=unique(stageCol), legend=sampleNames, pch=namesPch, x="bottomleft", cex=0.6, bty="n")

    return(pca)

}

# Actually plotting the PCA
pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_cpm.pca_clean.pdf"))
pcaresults <- plot.pca(cpmNormFilt, samplesMetaInd$stageCol, samplesMetaInd$pch, unique(samplesMetaInd$developmental_stage_s))
dev.off()

# Reproducibility (pretty dull with only one variable, but still)
plot.reproducibility <- function(data.to.test, metadata, method){
    cor.mat <- cor(data.to.test, method=method, use="pairwise.complete.obs")

    species.rep <- vector()
    between.species <- vector()

    for (i in 1:ncol(data.to.test)){
        for (j in 1:ncol(data.to.test)){
            if (j > i){
                if (metadata$developmental_stage_s[i] == metadata$developmental_stage_s[j]){
                    species.rep <- c(species.rep, cor.mat[i,j])
                } else {between.species <- c(between.species, cor.mat[i,j])}
            }
        }
    }

    for.plot <- list(species.rep, between.species)
    boxplot(for.plot, ylab=paste0(method, " correlation"), names=c("Within\ndays", "Between\ndays"))
}

pdf(file=paste0("eda_plots/", extraVars[1], "_", extraVars[2], "_reproducibility_by_levels_clean.pdf"))
plot.reproducibility(cpmNormFilt, samplesMetaInd, "spearman")
plot.reproducibility(cpmNormFilt, samplesMetaInd, "pearson")
dev.off()
