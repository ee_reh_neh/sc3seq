# For the mapping and comparative analysis of SC3-seq data from macFas blastocyst cells

## Table of contents:

* Basic set up
* Downloading the data
* Quality assessment
* Mapping
* Orthology filtering
* Other considerations
* Contact details


## Basic set up ##

These analyses use 

* Bash 
* R-3.2.2 (Bioconductor 3.2)
* featureCounts 1.5.0-p3 (standalone, not used within R)
* gcc 4.9.3 (module needs to be explicitly loaded)

It's set up for running on a PBS Pro cluster (NSCC). 

R Libraries and packages used:

* Rsubread 1.20.6
* limma
* edgeR
* plyr
* RColorBrewer 
* ggplot2 2.2.1
* reshape2 1.4.2

Genome builds are

* GRCh38p7
* macFas5

Ensembl version is 86 (Oct 2016)

## Downloading the data ##

Published data from [Nakamura et al](https://www.ncbi.nlm.nih.gov/pubmed/27556940) is available at the SRA under GEO Accession [GSE74767](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE74767); filtering runs in Runselector results in a sample sheet with 2526 runs across 421 individuals (390 single cells from blastocysts, 31 single cells from a macaque ESC pool)

This could have been wrapped into an array, buuuuuuut:

    for j in 100 200 300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 2100 2200 2300 2400 2500; do
        echo "IFS=\$'\n'
        for i in \`cat ~/pera_et_al/sample_sheets/runs_and_ids.txt | head -n $j | tail -n 100\`; do
                sra=\`echo \$i | awk '{print \$1}'\`; 
                dir=\`echo \$i | awk '{print \$2}'\`; 
                /home/users/ntu/igr/bin/sratoolkit.2.8.1-centos_linux64/bin/fastq-dump --gzip -C --outdir ~/pera_et_al/rawdata/\${dir} \$sra; 
                rm /home/users/ntu/igr/ncbi/public/sra/\${sra}*;
        done" | qsub -q normal -l walltime=24:00:00,ncpus=1 -o ~/pera_et_al/rawdata/downloads_${j}.out -N pera_${j} -M igr@ntu.edu.sg -m ae -V -j oe
    done

And then afterwards, for checking stuff...

IFS=$'\n';
    for j in `cat ~/pera_et_al/sample_sheets/runs_and_ids.txt`; do
        sra=`echo $j | awk '{print $1}'`; 
        dir=`echo $j | awk '{print $2}'`; 
        FILE=/home/users/ntu/igr/pera_et_al/rawdata/${dir}/${sra}.fastq.gz;
        echo $FILE;

        if [ -f $FILE ]
            then
                echo "$FILE already downloaded";
        else 
            echo "Downloading $FILE"
            /home/users/ntu/igr/bin/sratoolkit.2.8.1-centos_linux64/bin/fastq-dump --gzip -C --outdir ~/pera_et_al/rawdata/${dir} $sra; 
            rm /home/users/ntu/igr/ncbi/public/sra/${sra}*;
        fi
        done


## Quality assessment ##

Prior to mapping, reads were processed with [sickle](https://github.com/najoshi/sickle) to trim low quality bases and remove bad reads. I used default settings, but trimmed only from the 3' end, not the 5' end.

## Mapping ##

All data is colorspace SOLEXA runs generated through the [SC3-seq protocol](https://academic.oup.com/nar/article/43/9/e60/1112641/SC3-seq-a-method-for-highly-parallel-and), and mapped with Rsubread using scripts written by Othmar Korn. 

## Orthology filtering ##

Is much better detailed [here](http://www.bitbucket.org/ee_reh_neh/orthoexon)

## Other considerations ##

SC3-seq reads are generated from the poly-adenylated 3' ends of transcripts expressed in cells. The density of reads is not equally distributed throughout the transcript; rather, it peaks in a fairly narrow region ~150 bp upstream of the transcription end site (see figure 1D in the SC3-seq paper), and drops off very sharply as you keep going upstream from that. The human cell sequencing, however, even if enriched for coding mRNA through a poly-A pulldown step and strand-aware, is not going to have that same bias, but rather a different one. This presents massive problems for comparing across the two data sets. Chief amongst them, it will be pretty much impossible for you to confidently identify differentially expressed genes between the two species, because they are perfectly confounded with the two different technologies and you cannot disentangle one from the other. 

Additionally, if the exon that falls in the SC3-seq read peak is not orthologous you'll have widely diverging expression estimates; same if that exon is differentially spliced between species/is not constitutive, or if there's multiple transcription end sites, etc etc... In short, what I foresee is throwing out *a lot* of the data, and being very cautious with what remains. If I recall from our initial call, part of your interest in this data is identifying the macaque day/developmental stage that best matches the human PSC data, yes? That part should still mostly feasible, although I don't have a good a priori feel for how much resolution you'll manage to get - from what I gathered from the macaque paper, every time point is fairly distinct from the next, so we might get lucky. What I would do would be something like calculating RPKM across the two species (and we'll want to compare human RPKMs if calculated across the entire set of orthoexons vs the 3'-most orthoexon etc), merge them, and then pretty much regress principal components out of the data until interspecies/technological differences are minimised (this is similar to what is done in quantitative trait loci studies to maximise power), so that what remains are differences between developmental stage. Having done that, you can then try to build some sort of classifier from the macaque data and project the humans onto it, or similar.

## Contact details ##

Send all questions and queries and the such to igr at ntu dot edu dot sg.