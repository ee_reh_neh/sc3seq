#!/bin/bash
# From a script by John Blischak
# Modified by IGR to work on PBSPro (NSCC)
# 2017.02.04

set -e

FILE=$1
BASE=`basename ${FILE%.fastq.gz}`
OUTDIR=~/pera_et_al/sickle

mkdir -p $OUTDIR

if [ ! -s $FILE ]
then
  echo "File is missing or empty: $FILE"
  exit 65
fi

if [ -s $OUTDIR/$BASE.sickle.fastq.gz ]
then
  echo "Output file already exists: $OUTDIR/$BASE.sickle.fastq.gz"
  exit 64
fi

# Unzip file (throws error when passed unzipped file via process substitution)
zcat $FILE > $OUTDIR/$BASE.fastq

# Run sickle only cutting from the 3' end
sickle se -f $OUTDIR/$BASE.fastq -t sanger -o $OUTDIR/$BASE.sickle.fastq.gz -x -g

# Count number of reads
    size=`zcat $OUTDIR/$BASE.sickle.fastq.gz | wc -l | cut -f 1 -d ' '`
    echo $(($size/4)) > $BASE.sickle.txt

# Remove unzipped fastq file
rm $OUTDIR/$BASE.fastq
