rm(list=ls())
library(plyr)
library(ggplot2)
library(reshape2)

setwd("~/Dropbox/Data/sc3seq/")
featurecounts <- read.table("all_featurecounts.txt", header=F)
mapped <- read.table("all_mapped.txt", header=F)
sickle <- read.table("all_sickle.txt", header=F)
raw <- read.table("all_raw.txt", header=F)
readnames <- read.table("all_files.txt", header=F)
metadata <- read.table("runs_and_ids.txt", header=F)
readsfilt <- data.frame(readnames, raw, sickle, mapped)
readsfilt$V1 <- gsub("_fastqc\\.zip", "", readsfilt$V1)
readsfilt <- merge(readsfilt, featurecounts, by.x="V1", by.y="V1")

names(readsfilt) <- c("sraid", "raw", "sickle", "mapped", "featurecounts")

readsfilt <- merge(readsfilt, metadata, by.x="sraid", by.y="V1")
bySample <- ddply(readsfilt[,2:6], .(V2), function(x) colSums(x[,1:4]))
boxplot(bySample[,1:4], notch=T, names=c("Raw reads", "Pass read QC", "Mapped", "In ortho\nregions"))

# Violin plot with ggplot
ggplotdata <- melt(bySample)
ggplotdata$variable <- as.factor(ggplotdata$variable)

levels(ggplotdata$variable)[levels(ggplotdata$variable)=="raw"] <- "Raw"
levels(ggplotdata$variable)[levels(ggplotdata$variable)=="sickle"] <- "Pass read QC"
levels(ggplotdata$variable)[levels(ggplotdata$variable)=="mapped"] <- "Mapped (unlimited\nsoft clipping)"
levels(ggplotdata$variable)[levels(ggplotdata$variable)=="featurecounts"] <- "In orthologous\nregions"

pdf(file="eda_plots/read_avail.pdf")
ggplot(ggplotdata, aes(x=variable, y=value, fill=variable)) + geom_violin(trim=F) + geom_boxplot(width=0.1, fill="white") + scale_fill_manual(values=c("yellow3", "plum4", "firebrick", "dodgerblue3")) + theme_bw() + labs(title="", y="Reads per sample (million)", x="") + theme(legend.title=element_blank()) + scale_y_continuous(breaks=seq(0,12000000,1000000), labels=seq(0,12,1), limits=c(0,12000000))
dev.off()

