## ==========================================================================
## File:    subread_fastq_to_bam
## Author:  O.Korn
## Created: 2014-09-03
##
## Align reads (single end or paired end) to a given genome index.
## (The genome index must already exist).
##
## ==========================================================================


### Modified by IGR to work with NSCC and array set up:
### most variables hard-coded, could be easily turned into command line arguments. 
### 2017.02.05
###

### Command-line executable
### Requires 9 options!
### 1. input file path
### 2. input file name
### 3. index directory
### 4. index prefix
### 5. outdir
### 6. logical: convert colorspace to base? T/F
### 7. number cpus; optional
### 8. mismatches allowed; optional, defaults to 5
### 9. indels allowed; optional, defaults to 5

### command line example: 
### ~/bin/R-3.2.2./bin/Rscript --vanilla ~/repos/sc3seq/subread_align.r ~/pera_et_al/rawdata/tester/ SRR2930348.fastq.gz ~/pera_et_al/indexes macFas5 ~/pera_et_al/mapped/ T 2 2 2

.libPaths("~/R-3.2.2_libs")

library("Rsubread") # 1.20.6
library("limma") # 3.26.9
library("tools")

extraVars <- commandArgs(trailingOnly=T)

## NOTE: All input paths expected to be full ABSOLUTE paths as we are
##   required to set our working directory to within the genome index path.
inputdir <- extraVars[1]
inputfile <- extraVars[2]
indexdir <- extraVars[3]
indexname <- extraVars[4] 
outdir <- extraVars[5]
colorConvert <- extraVars[6]
ncpu <- extraVars[7]
mismatches <- extraVars[8]
indels <- extraVars[9]

## Default nthreads=2 if not given
if (is.na(ncpu)) {
  ncpu <- 2
} else {
  ncpu <- abs(as.integer(ncpu))
}

## Default mismatches (SNPs) = 5 if not given
if (is.na(mismatches)) {
  mismatches <- 5
} else {
  mismatches <- abs(as.integer(mismatches))
}
## Default indels = 5 if not given
if (is.na(indels)) {
  indels <- 5
} else {
  indels <- abs(as.integer(indels))
}

## Subread expects index files to exist in current working directory.
## We don't need to worry about relative paths for 'inputdir', 'outdir' etc
## because this script expects full absolute paths.

setwd(indexdir)

## Align paired end reads (output BAM)
  readfile1 <- paste0(inputdir, "/", inputfile)
  
  ## Work out if input is gzipped FASTQ or not
  input_format <- ifelse(grepl(".gz$", readfile1), "gzFASTQ", "FASTQ")

  ## Align single-end or paired-end reads (if readfile2 specified) to target genome index and output aligned BAM
  ## NOTE: By default, best unique mapping per read retained only (no multi-mapping)
  align(index=indexname,
        readfile1=readfile1,
        input_format=input_format,
        output_format="BAM",
        output_file=paste0(outdir, inputfile, ".bam"),
        unique=TRUE,
        nBestLocations=1,
        maxMismatches=mismatches,
        indels=indels,
        nthreads=ncpu,
        color2base=colorConvert)



