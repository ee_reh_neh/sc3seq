#!/bin/bash
# modified from a script by John Blischak (https://github.com/jdblischak/singleCellSeq/tree/master/code)

set -e

ANALYSIS=$1
FILES=${*:2}

NAME=`basename $ANALYSIS`
mkdir -p ~/pera_et_al/logs/$NAME

let numfiles=$#-1
echo $numfiles

echo "#! /bin/bash

files=( ${FILES[@]} )
let index=\$((\${PBS_ARRAY_INDEX}-1)) 

F=\${files[\$index]}

$ANALYSIS \$F

EXIT_CODE=\$?

if [ \$EXIT_CODE == 0 ]
then
  echo -e \"success\t\$F\"
elif [ \$EXIT_CODE == 64 ]
then
  # Output file already exists. $ANALYSIS logged error message.
  exit
elif [ \$EXIT_CODE == 65 ]
then
  # Input file was missing or empty. $ANALYSIS logged error message.
  exit
else
  echo -e \"failure\t\$F\"
fi
" | qsub -l walltime=1:00:00 -l select=1:mem=4gb:ncpus=1 -V -j oe -N $NAME -o ~/logs/$NAME -J 1-$numfiles -M igr@ntu.edu.sg -m ae
